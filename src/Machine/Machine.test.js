import React from 'react';
import ReactDOM from 'react-dom';
import Machine from './Machine';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Machine />, div);
  ReactDOM.unmountComponentAtNode(div);
});
