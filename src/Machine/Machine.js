import React, { Component } from 'react';
import './Machine.css';
import  Drumpad from '../Drumpad/Drumpad';
import Display from '../Display/Display';

class Machine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPad: null
    }

    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  handleClick(code) {
    let thisPad = this.props.drumpads[code];
    let clip = document.querySelector(`#${thisPad.name}`);
    clip.currentTime = 0;
    clip.play();
    this.setState({
      currentPad: thisPad.sound
    });
  }

  handleKeyPress(e) {
    let soundToFind;
    this.props.drumpads.forEach(pad => {
      if(e.keyCode === pad.code) {
        soundToFind = pad.name;
        let clip = document.querySelector(`#${soundToFind}`);
        clip.currentTime = 0;
        clip.play();
        this.setState({
          currentPad: pad.sound
        });
      }
    });
  }

  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyPress);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyPress);
  }

  render() {
    const drumpads = this.props.drumpads.map((pad, i) => {
      return <Drumpad 
                key={pad.code.toString()}
                soundId={pad.sound}
                value={pad.name}
                src={pad.src}
                onClick={() => this.handleClick(i)}
              />
    })
    
    return (
      <div id={this.props.id}>
        <div className="drum-bay">
          {drumpads}
        </div>
        <Display value={this.state.currentPad}/>
      </div>
    );
  }
}

export default Machine;
