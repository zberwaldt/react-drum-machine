import React, { Component } from 'react';
import './App.css';
import Machine from './Machine/Machine';

class App extends Component {
  constructor(props) {
    super(props);
    this.drumpads = [
      { 
        name : 'Q',
        code: 81,
        sound: "Heater-1",
        src: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3'
      },
      { 
        name : 'W',
        code: 87,
        sound: "Heater-2", 
        src: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-2.mp3'
      },
      { 
        name: 'E',
        code: 69,
        sound: "Heater-3",
        src: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3'
      },
      { 
        name : 'A',
        code: 65,
        sound: "Heater-4",
        src: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-4_1.mp3'
      },
      { 
        name : 'S',
        code: 83,
        sound: "Heater-6" ,
        src: 'https://s3.amazonaws.com/freecodecamp/drums/Heater-6.mp3'
      },
      { 
        name : 'D',
        code: 68,
        sound: "Dsc-Oh",
        src: 'https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3'
      },
      { 
        name : 'Z',
        code: 90,
        sound: "Kick-n-hat",
        src: 'https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3'
      },
      { 
        name : 'X',
        code: 88,
        sound: "Kick" ,
        src: 'https://s3.amazonaws.com/freecodecamp/drums/RP4_KICK_1.mp3'
      },
      { 
        name : 'C',
        code: 67,
        sound: "Closed-High-Hat",
        src: 'https://s3.amazonaws.com/freecodecamp/drums/Cev_H2.mp3'
      },
    ];
  }

  render() {

    return (
      <div className="App">
        <Machine id="drum-machine" drumpads={this.drumpads} />
      </div>
    );
  }
}

export default App;
