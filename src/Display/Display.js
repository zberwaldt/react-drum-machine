import React from 'react';
import './Display.css';

function Display(props) {
    return (
      <div className="display-bay">
        <h1>RDM-1000</h1>
        <p id="display">{ props.value }</p>
      </div>
    );
}

export default Display;
