import React from 'react';
import './Drumpad.css';

function Drumpad(props) {
    return (
      <button id={props.soundId}
              onClick={props.onClick} 
              className="drum-pad">
          <audio src={props.src} className="clip" id={props.value}></audio>
          { props.value }
      </button>
    );
}

export default Drumpad;
